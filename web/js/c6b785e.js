/**
 * Created by Honzik on 15.4.2015.
 */
$(document).ready(function () {


    var settings=$.totalStorage("navigation-settings");
    $("ul[class^='level-'] ").not('.level-0').hide();
    if(settings){
        for(var i= 0;i<settings.length;i++){
            var id=settings[i].id;
            var display=settings[i].display;
            $("#"+id).css("display",display);
        }
    }

    $("a").not("ul[class^='level-']>li>a").not(".noNavigationChange").on("click",function(){
        $.totalStorage("navigation-settings",null);
    });

    $("ul[class^='level-']>li>a").on("click",function(){
        $(this).addClass("selected");
        $(this).next().toggle();
        var id=$(this).next().attr("id");
        var display=$(this).next().css("display");
        if(id&&display){
            var styles= [];
            var prevStorage=$.totalStorage("navigation-settings");
            if(prevStorage){
                var found=false;
                for(var i=0;i<prevStorage.length;i++){
                    if(prevStorage[i]["id"]===id){
                        prevStorage[i]["display"]=display;
                        found=true;
                    }
                    styles.push(prevStorage[i]);
                }
                if(!found){
                    styles.push({
                        id:id,
                        display:display
                    });
                }
            }else{
                styles.push({
                    id:id,
                    display:display
                });
            }
            $.totalStorage("navigation-settings",styles);
        }
    });
});