<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Company
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Company
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Zadejte jméno společnosti")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ic", type="string", length=255)
     * @Assert\NotBlank(message="Zadejte ičo společnosti")
     */
    private $ic;

    /**
     * @var string
     *
     * @ORM\Column(name="dic", type="string", length=255, nullable=true)
     */
    private $dic;

    /**
     * @ORM\OneToOne(targetEntity="Purchase", inversedBy="company", cascade={"persist"})
     */
    protected $purchase;

    /**
 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="companies", cascade={"persist"})
 */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ic
     *
     * @param string $ic
     * @return Company
     */
    public function setIc($ic)
    {
        $this->ic = $ic;

        return $this;
    }

    /**
     * Get ic
     *
     * @return string 
     */
    public function getIc()
    {
        return $this->ic;
    }

    /**
     * Set dic
     *
     * @param string $dic
     * @return Company
     */
    public function setDic($dic)
    {
        $this->dic = $dic;

        return $this;
    }

    /**
     * Get dic
     *
     * @return string 
     */
    public function getDic()
    {
        return $this->dic;
    }

    /**
     * Set purchase
     *
     * @param \AppBundle\Entity\Purchase $purchase
     * @return Company
     */
    public function setPurchase(\AppBundle\Entity\Purchase $purchase = null)
    {
        $this->purchase = $purchase;

        return $this;
    }

    /**
     * Get purchase
     *
     * @return \AppBundle\Entity\Purchase 
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Company
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
