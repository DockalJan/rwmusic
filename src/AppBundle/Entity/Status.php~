<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Status
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Status
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * @ORM\OneToMany(mappedBy="status", targetEntity="AppBundle\Entity\PurchaseProduct", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $purchaseProducts;

    /**
     * @ORM\OneToMany(mappedBy="status", targetEntity="AppBundle\Entity\Purchase", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $purchases;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Status
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Status
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->purchaseProducts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->purchases = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add purchaseProducts
     *
     * @param \AppBundle\Entity\PurchaseProduct $purchaseProducts
     * @return Status
     */
    public function addPurchaseProduct(\AppBundle\Entity\PurchaseProduct $purchaseProducts)
    {
        $this->purchaseProducts[] = $purchaseProducts;

        return $this;
    }

    /**
     * Remove purchaseProducts
     *
     * @param \AppBundle\Entity\PurchaseProduct $purchaseProducts
     */
    public function removePurchaseProduct(\AppBundle\Entity\PurchaseProduct $purchaseProducts)
    {
        $this->purchaseProducts->removeElement($purchaseProducts);
    }

    /**
     * Get purchaseProducts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurchaseProducts()
    {
        return $this->purchaseProducts;
    }

    /**
     * Add purchases
     *
     * @param \AppBundle\Entity\Purchase $purchases
     * @return Status
     */
    public function addPurchase(\AppBundle\Entity\Purchase $purchases)
    {
        $this->purchases[] = $purchases;

        return $this;
    }

    /**
     * Remove purchases
     *
     * @param \AppBundle\Entity\Purchase $purchases
     */
    public function removePurchase(\AppBundle\Entity\Purchase $purchases)
    {
        $this->purchases->removeElement($purchases);
    }

    /**
     * Get purchases
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurchases()
    {
        return $this->purchases;
    }
}
