<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Purchase
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Purchase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime" , nullable=true)
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent", type="datetime" , nullable=true)
     */
    private $sent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ordered", type="datetime" , nullable=true)
     */
    private $ordered;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Status", inversedBy="purchases", cascade={"persist"})
     */
    private $status;

    /**
     * @ORM\OneToOne(targetEntity="Company", mappedBy="purchase", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true)
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Delivery", inversedBy="purchases", cascade={"persist"})
     */
    private $delivery;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment", inversedBy="purchases", cascade={"persist"})
     */
    private $payment;

    /**
     * @ORM\OneToOne(targetEntity="Contact", mappedBy="purchase", cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @ORM\OneToOne(targetEntity="Address", mappedBy="deliveryPurchase", cascade={"persist"})
     * @ORM\JoinColumn(name="delivery_address_id", referencedColumnName="id", nullable=true)
     */
    private $deliveryAddress;

    /**
     * @ORM\OneToOne(targetEntity="Address", mappedBy="invoicePurchase", cascade={"persist"})
     * @ORM\JoinColumn(name="invoice_address_id", referencedColumnName="id")
     */
    private $invoiceAddress;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PurchaseProduct", mappedBy="purchase", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $purProducts;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="purchases", cascade={"persist"})
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Purchase
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Purchase
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set sent
     *
     * @param \DateTime $sent
     * @return Purchase
     */
    public function setSent($sent)
    {
        $this->sent = $sent;

        return $this;
    }

    /**
     * Get sent
     *
     * @return \DateTime 
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set ordered
     *
     * @param \DateTime $ordered
     * @return Purchase
     */
    public function setOrdered($ordered)
    {
        $this->ordered = $ordered;

        return $this;
    }

    /**
     * Get ordered
     *
     * @return \DateTime 
     */
    public function getOrdered()
    {
        return $this->ordered;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Company $company
     * @return Purchase
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set delivery
     *
     * @param \AppBundle\Entity\Delivery $delivery
     * @return Purchase
     */
    public function setDelivery(\AppBundle\Entity\Delivery $delivery = null)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * Get delivery
     *
     * @return \AppBundle\Entity\Delivery 
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set payment
     *
     * @param \AppBundle\Entity\Payment $payment
     * @return Purchase
     */
    public function setPayment(\AppBundle\Entity\Payment $payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return \AppBundle\Entity\Payment 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set contact
     *
     * @param \AppBundle\Entity\Contact $contact
     * @return Purchase
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \AppBundle\Entity\Contact 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set deliveryAddress
     *
     * @param \AppBundle\Entity\Address $deliveryAddress
     * @return Purchase
     */
    public function setDeliveryAddress(Address $deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Get deliveryAddress
     *
     * @return \AppBundle\Entity\Address 
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Set invoiceAddress
     *
     * @param \AppBundle\Entity\Address $invoiceAddress
     * @return Purchase
     */
    public function setInvoiceAddress(Address $invoiceAddress = null)
    {
        $this->invoiceAddress = $invoiceAddress;
        return $this;
    }

    /**
     * Get invoiceAddress
     *
     * @return \AppBundle\Entity\Address 
     */
    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->purProducts = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Get purProducts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurProducts()
    {
        return $this->purProducts;
    }



    /**
     * Set status
     *
     * @param \AppBundle\Entity\Status $status
     * @return Purchase
     */
    public function setStatus(\AppBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\Status 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Purchase
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add purProducts
     *
     * @param \AppBundle\Entity\PurchaseProduct $purProducts
     * @return Purchase
     */
    public function addPurProduct(\AppBundle\Entity\PurchaseProduct $purProducts)
    {
        $this->purProducts[] = $purProducts;

        return $this;
    }

    /**
     * Remove purProducts
     *
     * @param \AppBundle\Entity\PurchaseProduct $purProducts
     */
    public function removePurProduct(\AppBundle\Entity\PurchaseProduct $purProducts)
    {
        $this->purProducts->removeElement($purProducts);
    }
}
