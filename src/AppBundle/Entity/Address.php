<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Address
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     * @Assert\NotBlank(message="Křestní jméno musí být vyplněné")
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     * @Assert\NotBlank(message="Příjmení musí být vyplněné")
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     * @Assert\NotBlank(message="Město musí být vyplněné")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     * @Assert\NotBlank(message="Ulice musí být vyplněná")
     */
    private $street;

    /**
     * @var integer
     *
     * @ORM\Column(name="zipCode", type="integer", length=255)
     * @Assert\Length(min="5", max="5", exactMessage="Směrovací číslo musí obsahovat 5 číslic")
     */
    private $zipCode;

    /**
     * @ORM\OneToOne(targetEntity="Purchase", inversedBy="deliveryAddress", cascade={"persist"})
     */
    private $deliveryPurchase;

    /**
     * @ORM\OneToOne(targetEntity="Purchase", inversedBy="invoiceAddress", cascade={"persist"})
     */
    private $invoicePurchase;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="deliveryAddress", cascade={"persist"})
     */
    private $deliveryUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="invoiceAddress", cascade={"persist"})
     */
    private $invoiceUser;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Address
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Address
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return Address
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set deliveryPurchase
     *
     * @param \AppBundle\Entity\Purchase $deliveryPurchase
     * @return Address
     */
    public function setDeliveryPurchase(\AppBundle\Entity\Purchase $deliveryPurchase = null)
    {
        $this->deliveryPurchase = $deliveryPurchase;

        return $this;
    }

    /**
     * Get deliveryPurchase
     *
     * @return \AppBundle\Entity\Purchase 
     */
    public function getDeliveryPurchase()
    {
        return $this->deliveryPurchase;
    }

    /**
     * Set invoicePurchase
     *
     * @param \AppBundle\Entity\Purchase $invoicePurchase
     * @return Address
     */
    public function setInvoicePurchase(\AppBundle\Entity\Purchase $invoicePurchase = null)
    {
        $this->invoicePurchase = $invoicePurchase;

        return $this;
    }

    /**
     * Get invoicePurchase
     *
     * @return \AppBundle\Entity\Purchase 
     */
    public function getInvoicePurchase()
    {
        return $this->invoicePurchase;
    }

    /**
     * Set deliveryUser
     *
     * @param \AppBundle\Entity\User $deliveryUser
     * @return Address
     */
    public function setDeliveryUser(\AppBundle\Entity\User $deliveryUser = null)
    {
        $this->deliveryUser = $deliveryUser;

        return $this;
    }

    /**
     * Get deliveryUser
     *
     * @return \AppBundle\Entity\User 
     */
    public function getDeliveryUser()
    {
        return $this->deliveryUser;
    }

    /**
     * Set invoiceUser
     *
     * @param \AppBundle\Entity\User $invoiceUser
     * @return Address
     */
    public function setInvoiceUser(\AppBundle\Entity\User $invoiceUser = null)
    {
        $this->invoiceUser = $invoiceUser;

        return $this;
    }

    /**
     * Get invoiceUser
     *
     * @return \AppBundle\Entity\User 
     */
    public function getInvoiceUser()
    {
        return $this->invoiceUser;
    }
}
