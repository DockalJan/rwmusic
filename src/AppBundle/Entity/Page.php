<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;

/**
 * Page
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Page
{
    const ENUM_ARTICLE="article";
    const ENUM_CATEGORY="category";
    const ENUM_PRODUCT="product";

    /**
     * @ORM\OneToOne(targetEntity="Article", inversedBy="page", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $article;

    /**
     * @ORM\OneToOne(targetEntity="Category", inversedBy="page", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $category;

    /**
     * @ORM\OneToOne(targetEntity="Product", inversedBy="page", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $product;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, unique=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Page
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Page
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Page
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }




    public function getPageType(){
        if(self::ENUM_ARTICLE){
            return $this->getArticle();
        }elseif(self::ENUM_PRODUCT){
            return $this->getProduct();
        }elseif(self::ENUM_CATEGORY){
            return $this->getCategory();
        }
        throw new ParameterNotFoundException("Vazba vztahující se k Page nebyla nalezena");
    }

    public function setPageType($type){
        switch ($type){
            case self::ENUM_ARTICLE:
                $this->setProduct(null);
                $this->setCategory(null);
                $this->setArticle($type);
                return $this;
            case self::ENUM_PRODUCT:
                $this->setArticle(null);
                $this->setCategory(null);
                $this->setProduct($type);
                return $this;
            case self::ENUM_CATEGORY:
                $this->setArticle(null);
                $this->setProduct(null);
                $this->setCategory($type);
                return $this;
            default:
                throw new \InvalidArgumentException("Invalid Status");
        }
    }

    /**
     * Set article
     *
     * @param \AppBundle\Entity\Article $article
     * @return Page
     */
    public function setArticle(\AppBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \AppBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     * @return Page
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return Page
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
