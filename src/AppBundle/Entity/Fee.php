<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fee
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Fee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var double
     *
     * @ORM\Column(name="price", type="decimal", scale=2)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="string", length=255)
     */
    private $shortDescription;

    /**
     * @ORM\ManyToOne(targetEntity="Tax", inversedBy="fees", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $tax;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ExtraFee", mappedBy="fee", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $extraFees;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set price
     *
     * @param string $price
     * @return Fee
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Fee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Fee
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set tax
     *
     * @param \AppBundle\Entity\Tax $tax
     * @return Fee
     */
    public function setTax(\AppBundle\Entity\Tax $tax = null)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return \AppBundle\Entity\Tax 
     */
    public function getTax()
    {
        return $this->tax;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add products
     *
     * @param \AppBundle\Entity\Product $products
     * @return Fee
     */
    public function addProduct(\AppBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \AppBundle\Entity\Product $products
     */
    public function removeProduct(\AppBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    public function getPriceWithTax(){
        if($this->getTax()){
            return ($this->getTax()->getRate()+1)*$this->getPrice();
        }
        return $this->getPrice();
    }


    /**
     * Add extraFees
     *
     * @param \AppBundle\Entity\ExtraFee $extraFees
     * @return Fee
     */
    public function addExtraFee(\AppBundle\Entity\ExtraFee $extraFees)
    {
        $this->extraFees[] = $extraFees;

        return $this;
    }

    /**
     * Remove extraFees
     *
     * @param \AppBundle\Entity\ExtraFee $extraFees
     */
    public function removeExtraFee(\AppBundle\Entity\ExtraFee $extraFees)
    {
        $this->extraFees->removeElement($extraFees);
    }

    /**
     * Get extraFees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExtraFees()
    {
        return $this->extraFees;
    }
}
