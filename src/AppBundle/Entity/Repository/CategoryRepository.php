<?php
/**
 * Created by PhpStorm.
 * User: Honzik
 * Date: 11.4.2015
 * Time: 22:17
 */

namespace AppBundle\Entity\Repository;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class CategoryRepository extends NestedTreeRepository
{

    public function getNavigation(){
        /*
         *


        return $this->childrenHierarchy(
            null,
            false,
            array(
                'decorate' => true,
                'rootOpen' => function($node){
                    $level="level-".$node[0]["lvl"];
                    if($node[0]["lvl"]!=0){
                        return "<ul id='category-".$node[0]['id']."' style='display:none;' class='".$level."'>";
                    }
                    return "<ul id='category-".$node[0]['id']."' class='".$level."'>";
                },
                'rootClose' => '</ul>',
                'childOpen' => '<li>',
                'childClose' => '</li>',
                'nodeDecorator' => function($node) {
                    $category=$this->find($node["id"]);
                    $pathTree=$this->getPath($category);
                    $pathUrl="";
                    foreach($pathTree as $value){
                        $page=$value->getPage();
                        if(!$page){
                            throw new NotFoundHttpException("Není nalezen záznam o stánce na tuto kategorii: ".$node["name"]);
                        }
                        $pathUrl.="/".$page->getAddress();
                    }
                    return '<a href="/kategorie'.$pathUrl.'/3/0">'.$node["name"].'</a>';
                }
            )
        );
                 */
    }

}