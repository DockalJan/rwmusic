<?php
/**
 * Created by PhpStorm.
 * User: Honzik
 * Date: 12.4.2015
 * Time: 0:46
 */

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{

    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_USER');
    }
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Address", mappedBy="deliveryUser", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $deliveryAddress;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Address", mappedBy="invoiceUser", cascade={"persist"})
     * @ORM\JoinColumn(name="invoice_address_id", referencedColumnName="id")
     */
    private $invoiceAddress;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Contact", mappedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Company", mappedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $companies;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Purchase", mappedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="purchase_id", referencedColumnName="id")
     */
    private $purchases;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }







    /**
     * Add contact
     *
     * @param \AppBundle\Entity\Contact $contact
     * @return User
     */
    public function addContact(\AppBundle\Entity\Contact $contact)
    {
        $this->contact[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \AppBundle\Entity\Contact $contact
     */
    public function removeContact(\AppBundle\Entity\Contact $contact)
    {
        $this->contact->removeElement($contact);
    }


    /**
     * Add deliveryAddress
     *
     * @param \AppBundle\Entity\Address $deliveryAddress
     * @return User
     */
    public function addDeliveryAddress(\AppBundle\Entity\Address $deliveryAddress)
    {
        $this->deliveryAddress[] = $deliveryAddress;

        return $this;
    }

    /**
     * Remove deliveryAddress
     *
     * @param \AppBundle\Entity\Address $deliveryAddress
     */
    public function removeDeliveryAddress(\AppBundle\Entity\Address $deliveryAddress)
    {
        $this->deliveryAddress->removeElement($deliveryAddress);
    }

    /**
     * Get deliveryAddress
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Add invoiceAddress
     *
     * @param \AppBundle\Entity\Address $invoiceAddress
     * @return User
     */
    public function addInvoiceAddress(\AppBundle\Entity\Address $invoiceAddress)
    {
        $this->invoiceAddress[] = $invoiceAddress;

        return $this;
    }

    /**
     * Remove invoiceAddress
     *
     * @param \AppBundle\Entity\Address $invoiceAddress
     */
    public function removeInvoiceAddress(\AppBundle\Entity\Address $invoiceAddress)
    {
        $this->invoiceAddress->removeElement($invoiceAddress);
    }

    /**
     * Get invoiceAddress
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add companies
     *
     * @param \AppBundle\Entity\Company $companies
     * @return User
     */
    public function addCompany(\AppBundle\Entity\Company $companies)
    {
        $this->companies[] = $companies;

        return $this;
    }

    /**
     * Remove companies
     *
     * @param \AppBundle\Entity\Company $companies
     */
    public function removeCompany(\AppBundle\Entity\Company $companies)
    {
        $this->companies->removeElement($companies);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Add purchases
     *
     * @param \AppBundle\Entity\Purchase $purchases
     * @return User
     */
    public function addPurchase(\AppBundle\Entity\Purchase $purchases)
    {
        $this->purchases[] = $purchases;

        return $this;
    }

    /**
     * Remove purchases
     *
     * @param \AppBundle\Entity\Purchase $purchases
     */
    public function removePurchase(\AppBundle\Entity\Purchase $purchases)
    {
        $this->purchases->removeElement($purchases);
    }

    /**
     * Get purchases
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurchases()
    {
        return $this->purchases;
    }
}
