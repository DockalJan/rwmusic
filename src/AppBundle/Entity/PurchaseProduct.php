<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PurchaseProduct
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PurchaseProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="unitCount", type="integer")
     */
    private $unitCount;

    /**
     * @var string
     *
     * @ORM\Column(name="unitPriceNoTax", type="decimal", scale=2)
     */
    private $unitPriceNoTax;

    /**
     * @var string
     *
     * @ORM\Column(name="includeFeesNoTax", type="decimal", scale=2)
     */
    private $includeFeesNoTax;

    /**
     * @var string
     *
     * @ORM\Column(name="includeFeesWithTax", type="decimal", scale=2)
     */
    private $includeFeesWithTax;

    /**
     * @var string
     *
     * @ORM\Column(name="taxRate", type="decimal", scale=2, nullable=true)
     */
    private $taxRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="warranty", type="decimal", scale=2)
     */
    private $warranty;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="string", length=255)
     */
    private $shortDescription;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="purProducts", cascade={"persist"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Status", inversedBy="purchaseProducts", cascade={"persist"})
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Purchase", inversedBy="purProducts", cascade={"persist"})
     */
    private $purchase;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unitCount
     *
     * @param integer $unitCount
     * @return PurchaseProduct
     */
    public function setUnitCount($unitCount)
    {
        $this->unitCount = $unitCount;

        return $this;
    }

    /**
     * Get unitCount
     *
     * @return integer 
     */
    public function getUnitCount()
    {
        return $this->unitCount;
    }

    /**
     * Set unitPriceNoTax
     *
     * @param string $unitPriceNoTax
     * @return PurchaseProduct
     */
    public function setUnitPriceNoTax($unitPriceNoTax)
    {
        $this->unitPriceNoTax = $unitPriceNoTax;

        return $this;
    }

    /**
     * Get unitPriceNoTax
     *
     * @return string 
     */
    public function getUnitPriceNoTax()
    {
        return $this->unitPriceNoTax;
    }

    /**
     * Set includeFeesNoTax
     *
     * @param string $includeFeesNoTax
     * @return PurchaseProduct
     */
    public function setIncludeFeesNoTax($includeFeesNoTax)
    {
        $this->includeFeesNoTax = $includeFeesNoTax;

        return $this;
    }

    /**
     * Get includeFeesNoTax
     *
     * @return string 
     */
    public function getIncludeFeesNoTax()
    {
        return $this->includeFeesNoTax;
    }

    /**
     * Set includeFeesWithTax
     *
     * @param string $includeFeesWithTax
     * @return PurchaseProduct
     */
    public function setIncludeFeesWithTax($includeFeesWithTax)
    {
        $this->includeFeesWithTax = $includeFeesWithTax;

        return $this;
    }

    /**
     * Get includeFeesWithTax
     *
     * @return string 
     */
    public function getIncludeFeesWithTax()
    {
        return $this->includeFeesWithTax;
    }

    /**
     * Set taxRate
     *
     * @param string $taxRate
     * @return PurchaseProduct
     */
    public function setTaxRate($taxRate)
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    /**
     * Get taxRate
     *
     * @return string 
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * Set warranty
     *
     * @param integer $warranty
     * @return PurchaseProduct
     */
    public function setWarranty($warranty)
    {
        $this->warranty = $warranty;

        return $this;
    }

    /**
     * Get warranty
     *
     * @return integer 
     */
    public function getWarranty()
    {
        return $this->warranty;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PurchaseProduct
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PurchaseProduct
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return PurchaseProduct
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return PurchaseProduct
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set purchase
     *
     * @param \AppBundle\Entity\Purchase $purchase
     * @return PurchaseProduct
     */
    public function setPurchase(\AppBundle\Entity\Purchase $purchase = null)
    {
        $this->purchase = $purchase;

        return $this;
    }

    /**
     * Get purchase
     *
     * @return \AppBundle\Entity\Purchase 
     */
    public function getPurchase()
    {
        return $this->purchase;
    }


    /**
     * Set status
     *
     * @param \AppBundle\Entity\Status $status
     * @return PurchaseProduct
     */
    public function setStatus(\AppBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\Status 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
