<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraFee
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ExtraFee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var double
     *
     * @ORM\Column(name="price", type="decimal", scale=2)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fee", inversedBy="extraFees", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */

    private $fee;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="extraFees", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */

    private $product;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param double $price
     * @return ExtraFee
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set fee
     *
     * @param \AppBundle\Entity\Fee $fee
     * @return ExtraFee
     */
    public function setFee(\AppBundle\Entity\Fee $fee = null)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee
     *
     * @return \AppBundle\Entity\Fee 
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return ExtraFee
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }


    public function getPriceNoTax(){
        return $this->getFee()->getPrice()+$this->getPrice();
    }

    public function getPriceWithTax(){
        return $this->getFee()->getPriceWithTax()+$this->getPrice();
    }
}
