<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tax
 *
 * @ORM\Table(name="tax")
 * @ORM\Entity
 */
class Tax
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var double
     *
     * @ORM\Column(name="rate", type="decimal", scale=2)
     */
    private $rate;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Product", mappedBy="tax", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Fee", mappedBy="tax", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $fees;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Delivery", mappedBy="tax", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $deliveries;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Payment", mappedBy="tax", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $payments;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tax
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rate
     *
     * @param string $rate
     * @return Tax
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add products
     *
     * @param \AppBundle\Entity\Product $products
     * @return Tax
     */
    public function addProduct(\AppBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \AppBundle\Entity\Product $products
     */
    public function removeProduct(\AppBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add fees
     *
     * @param \AppBundle\Entity\Fee $fees
     * @return Tax
     */
    public function addFee(\AppBundle\Entity\Fee $fees)
    {
        $this->fees[] = $fees;

        return $this;
    }

    /**
     * Remove fees
     *
     * @param \AppBundle\Entity\Fee $fees
     */
    public function removeFee(\AppBundle\Entity\Fee $fees)
    {
        $this->fees->removeElement($fees);
    }

    /**
     * Get fees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * Add deliveries
     *
     * @param \AppBundle\Entity\Delivery $deliveries
     * @return Tax
     */
    public function addDelivery(\AppBundle\Entity\Delivery $deliveries)
    {
        $this->deliveries[] = $deliveries;

        return $this;
    }

    /**
     * Remove deliveries
     *
     * @param \AppBundle\Entity\Delivery $deliveries
     */
    public function removeDelivery(\AppBundle\Entity\Delivery $deliveries)
    {
        $this->deliveries->removeElement($deliveries);
    }

    /**
     * Get deliveries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeliveries()
    {
        return $this->deliveries;
    }

    /**
     * Add payments
     *
     * @param \AppBundle\Entity\Payment $payments
     * @return Tax
     */
    public function addPayment(\AppBundle\Entity\Payment $payments)
    {
        $this->payments[] = $payments;

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \AppBundle\Entity\Payment $payments
     */
    public function removePayment(\AppBundle\Entity\Payment $payments)
    {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments()
    {
        return $this->payments;
    }
}
