<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="string", length=255)
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="packetContent", type="string", length=255)
     */
    private $packetContent;

    /**
     * @var double
     *
     * @ORM\Column(name="price", type="decimal", scale=2)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="warranty", type="integer")
     */
    private $warranty;


    /**
     * @var boolean
     *
     * @ORM\Column(name="displayed", type="boolean")
     */
    private $displayed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="flagNew", type="boolean", nullable=true)
     */
    private $flagNew;

    /**
     * @var boolean
     *
     * @ORM\Column(name="flagAction", type="boolean", nullable=true)
     */
    private $flagAction;

    /**
     * @var boolean
     *
     * @ORM\Column(name="flagSale", type="boolean", nullable=true)
     */
    private $flagSale;

    /**
     * @var boolean
     *
     * @ORM\Column(name="customFlag", type="boolean", nullable=true)
     */
    private $customFlag;

    /**
     * @ORM\ManyToOne(targetEntity="Manufacturer", inversedBy="products", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $manufacturer;

    /**
     * @ORM\ManyToOne(targetEntity="Tax", inversedBy="products", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $tax;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image", mappedBy="product", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Category", inversedBy="products", cascade={"persist"})
     * @ORM\JoinTable(name="category_product",
     * joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $categories;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ExtraFee", mappedBy="product", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $extraFees;

    /**
     * @ORM\OneToOne(targetEntity="Page", mappedBy="product", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $page;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PurchaseProduct", mappedBy="product", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $purProducts;

    public function __toString(){
        return "truda";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Product
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set packetContent
     *
     * @param string $packetContent
     * @return Product
     */
    public function setPacketContent($packetContent)
    {
        $this->packetContent = $packetContent;

        return $this;
    }

    /**
     * Get packetContent
     *
     * @return string 
     */
    public function getPacketContent()
    {
        return $this->packetContent;
    }

    /**
     * Set price
     *
     * @param double $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }




    /**
     * Set displayed
     *
     * @param boolean $displayed
     * @return Product
     */
    public function setDisplayed($displayed)
    {
        $this->displayed = $displayed;

        return $this;
    }

    /**
     * Get displayed
     *
     * @return boolean 
     */
    public function getDisplayed()
    {
        return $this->displayed;
    }

    /**
     * Set flagNew
     *
     * @param string $flagNew
     * @return Product
     */
    public function setFlagNew($flagNew)
    {
        $this->flagNew = $flagNew;

        return $this;
    }

    /**
     * Get flagNew
     *
     * @return string 
     */
    public function getFlagNew()
    {
        return $this->flagNew;
    }

    /**
     * Set flagAction
     *
     * @param string $flagAction
     * @return Product
     */
    public function setFlagAction($flagAction)
    {
        $this->flagAction = $flagAction;

        return $this;
    }

    /**
     * Get flagAction
     *
     * @return string 
     */
    public function getFlagAction()
    {
        return $this->flagAction;
    }

    /**
     * Set flagSale
     *
     * @param string $flagSale
     * @return Product
     */
    public function setFlagSale($flagSale)
    {
        $this->flagSale = $flagSale;

        return $this;
    }

    /**
     * Get flagSale
     *
     * @return string 
     */
    public function getFlagSale()
    {
        return $this->flagSale;
    }

    /**
     * Set customFlag
     *
     * @param string $customFlag
     * @return Product
     */
    public function setCustomFlag($customFlag)
    {
        $this->customFlag = $customFlag;

        return $this;
    }

    /**
     * Get customFlag
     *
     * @return string 
     */
    public function getCustomFlag()
    {
        return $this->customFlag;
    }

    public function getAddress(){
        $this->getCategories();
    }

    /**
     * Set manufacturer
     *
     * @param \AppBundle\Entity\Manufacturer $manufacturer
     * @return Product
     */
    public function setManufacturer(\AppBundle\Entity\Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return \AppBundle\Entity\Manufacturer 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }


    /**
     * Add images
     *
     * @param \AppBundle\Entity\Image $images
     * @return Product
     */
    public function addImage(\AppBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \AppBundle\Entity\Image $images
     */
    public function removeImage(\AppBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add categories
     *
     * @param \AppBundle\Entity\Category $categories
     * @return Product
     */
    public function addCategory(\AppBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \AppBundle\Entity\Category $categories
     */
    public function removeCategory(\AppBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set tax
     *
     * @param \AppBundle\Entity\Tax $tax
     * @return Product
     */
    public function setTax(\AppBundle\Entity\Tax $tax = null)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return \AppBundle\Entity\Tax 
     */
    public function getTax()
    {
        return $this->tax;
    }



    public function getTotalFeesNoTax(){
        $total=0;
        $fees=$this->getExtraFees();
        foreach($fees as $fee){
            $total=+$fee->getPriceNoTax();
        }
        return $total;
    }

    public function getTotalFeesWithTax(){
        $total=0;
        $fees=$this->getExtraFees();
        foreach($fees as $fee){
            $total=+$fee->getPriceWithTax();
        }
        return $total;
    }

    public function getPriceWithTax(){
        if(!$this->getTax()){
            return $this->getPrice();
        }
        return ($this->getTax()->getRate()+1)*$this->getPrice();
    }

    public function getTotalPriceNoTax(){
        return $this->getTotalFeesNoTax()+$this->getPrice();
    }

    public function getTotalPriceWithTax(){
        return $this->getTotalFeesWithTax()+$this->getPriceWithTax();
    }

    public function getMainImage(){
        foreach($this->getImages() as $image){
            if($image->getPlacings()===1){
                return $image->getName();
            }
        }
        return "no-image-small.jpg";
    }


    /**
     * Set page
     *
     * @param \AppBundle\Entity\Page $page
     * @return Product
     */
    public function setPage(\AppBundle\Entity\Page $page = null)
    {
        $this->page = $page;
        $page->setPageType("product");
        return $this;
    }

    /**
     * Get page
     *
     * @return \AppBundle\Entity\Page 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Add purProducts
     *
     * @param \AppBundle\Entity\PurchaseProduct $purProducts
     * @return Product
     */
    public function addPurProduct(PurchaseProduct $purProducts)
    {
        $this->purProducts[] = $purProducts;

        return $this;
    }

    /**
     * Remove purProducts
     *
     * @param \AppBundle\Entity\PurchaseProduct $purProducts
     */
    public function removePurProduct(PurchaseProduct $purProducts)
    {
        $this->purProducts->removeElement($purProducts);
    }

    /**
     * Get purProducts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurProducts()
    {
        return $this->purProducts;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->extraFees = new \Doctrine\Common\Collections\ArrayCollection();
        $this->purProducts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set warranty
     *
     * @param integer $warranty
     * @return Product
     */
    public function setWarranty($warranty)
    {
        $this->warranty = $warranty;

        return $this;
    }

    /**
     * Get warranty
     *
     * @return integer 
     */
    public function getWarranty()
    {
        return $this->warranty;
    }

    /**
     * Add extraFees
     *
     * @param \AppBundle\Entity\ExtraFee $extraFees
     * @return Product
     */
    public function addExtraFee(\AppBundle\Entity\ExtraFee $extraFees)
    {
        $this->extraFees[] = $extraFees;

        return $this;
    }

    /**
     * Remove extraFees
     *
     * @param \AppBundle\Entity\ExtraFee $extraFees
     */
    public function removeExtraFee(\AppBundle\Entity\ExtraFee $extraFees)
    {
        $this->extraFees->removeElement($extraFees);
    }

    /**
     * Get extraFees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExtraFees()
    {
        return $this->extraFees;
    }

}
