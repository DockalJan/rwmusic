<?php
/**
 * Created by PhpStorm.
 * User: Honzik
 * Date: 17.4.2015
 * Time: 14:20
 */
namespace AppBundle\Twig;

use AppBundle\DependencyInjection\NavigationManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Třída rozšiřující funkce šablonovací jazyka Twig
 * Class AppExtension
 * @package AppBundle\Twig
 */
class AppExtension extends \Twig_Extension{

    protected $nm;
    private $kernel;

    public function __construct(NavigationManager $nm,KernelInterface $kernel){
        $this->nm=$nm;
        $this->kernel = $kernel;
    }

    /**
     * Definuje nové funkce
     * @return array
     */
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction("getNavigation",array($this,"getCustomNavigationBar")),
            new \Twig_SimpleFunction("assetExist",array($this,"assetExist")),
        );
    }

    /**
     * Na předchozí definované funkce je tato metoda její příslušné akce
     * @return array|string
     */
    public function getCustomNavigationBar(){
        return $this->nm->createHtmlNavigation();
    }

    /**
     *  Na předchozí definované funkce je tato metoda její příslušné akce
     * @param $path
     * @return bool
     */
    public function assetExist($path){
        $webRoot = realpath($this->kernel->getRootDir() . '/../web/');
        $toCheck = $webRoot."/" . $path;
        // check if the file exists
        if (!is_file($toCheck))
        {
            return false;
        }

        // check if file is well contained in web/ directory (prevents ../ in paths)
        if (strncmp($webRoot, $toCheck, strlen($webRoot)) !== 0)
        {
            return false;
        }

        return true;
    }

    /**
     * Unikátní jméno rozšíření
     * @return string
     */
    public function getName(){
        return "app_navigation_bar";
    }
}