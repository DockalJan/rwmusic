<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AjaxControllerTest extends WebTestCase
{
    public function testDelivery()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'ajax/delivery');
    }

    public function testInvoice()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'ajax/invoice');
    }

    public function testContact()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'ajax/contact');
    }

    public function testCompany()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'ajaxCompany');
    }

}
