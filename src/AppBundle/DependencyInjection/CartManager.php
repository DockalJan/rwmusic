<?php
/**
 * Created by PhpStorm.
 * User: Honzik
 * Date: 15.4.2015
 * Time: 0:16
 */
namespace AppBundle\DependencyInjection;

use AppBundle\Entity\Address;
use AppBundle\Entity\Purchase;
use AppBundle\Entity\PurchaseProduct;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Třída obshluhuje práci s košíkem
 * Class CartManager
 * @package AppBundle\DependencyInjection
 */
class CartManager {
    protected $session;
    protected $em;
    protected $productRepo;
    const SESSION_CART="SESSION_CART";


    public function __construct(Session $session, EntityManager $em){
        $this->session=$session;
        $this->em=$em;
        $this->productRepo=$em->getRepository("AppBundle:Product");
    }

    /**
     * Přidává produkt a jeho množstí do proměnné session
     * @param $product
     * @param $quantity
     * @return array|mixed
     */
    public function addProduct($product,$quantity){
        $products=$this->getProducts();
        $this->session->getFlashBag()->add(
            "success",
            "Úspešně jste přidali  "
            .$this->productRepo->find($product)->getName().
            " do košíku "
        );
        if($this->hasProduct($product)){
            $this->setQuantity($product,0,true);
            return $this->getProducts();
        }
        array_push($products,array("product"=>$product,"quantity"=>$quantity));
        $this->session->set(self::SESSION_CART,$products);
        return  $this->getProducts();
    }

    /**
     * Odstraňuje produkty z proměnné session
     * @param $product
     */
    public function removeProduct($product){
        $products=$this->getProducts();
        foreach($products as $key=>$prod){
            if($prod["product"]===$product){
                $this->session->getFlashBag()->add(
                    "success",
                    "Úspešně jste odebrali  "
                    .$this->productRepo->find($product)->getName().
                    " z košíku "
                );
                unset ($products[$key]);
                $this->session->set(self::SESSION_CART,$products);
                break;
            }
        }
    }

    /**
     * Nastavuje počer produktů daného produktu
     * @param $product
     * @param int $quantity
     * @param bool $plusOne
     */
    public function setQuantity($product,$quantity=0,$plusOne=false){
        $products=$this->getProducts();
        $editKey="";
        $editProduct=array();
        foreach($products as $key=>$prod){
            if($prod["product"]===$product){
                if($plusOne){
                    $prod["quantity"]+=$quantity;
                }else{
                    $this->session->getFlashBag()->add(
                        "success",
                        "Úspešně uměnili počet produktů "
                        .$this->productRepo->find($product)->getName().
                        " z ".$prod["quantity"]."ks na ".$quantity." ks"
                    );
                    $prod["quantity"]=$quantity;
                }
                $editKey=$key;
                $editProduct=$prod;
            };
        }
        $products[$editKey]=$editProduct;
        $this->session->set(self::SESSION_CART,$products);
    }

    /**
     * Metoda hledá, jestli neexistuje v session stejný produkt
     * @param $product
     * @return bool
     */
    public function hasProduct($product){
        $products=$this->getProducts();
        foreach($products as $prod){
            if($prod["product"]===$product){
                return true;
            };
        }
        return false;
    }

    /**
     * Metoda vrací jestli je košík prázdný
     * @return bool
     */
    public function isEmpty(){
        if(sizeof($this->getProducts())==0){
            return true;
        }
        return false;
    }

    /**
     * vrací veškeré prodkuty v košíku
     * @return array|mixed
     */
    public function getProducts(){
        $products=$this->session->get(self::SESSION_CART);
        if(!$products){
            return array();
        }
        return $products;
    }


    /**
     * Vytváří záznam v databázi o příslušné objednávce.
     * Pracuje především s entitou PurchaseProduct
     *
     * @param Purchase $purchase, User $user
     * @return PurchaseProduct
     */
    public function setPurchaseProduct(Purchase $purchase){
        foreach($this->getProducts() as $prod){
            $purProd=new PurchaseProduct();
            $product=$this->em->getRepository("AppBundle:Product")->find($prod["product"]);
            $purProd->setProduct($product);
            $purProd->setUnitCount($prod["quantity"]);
            $purProd->setDescription($product->getDescription());
            $purProd->setIncludeFeesNoTax($product->getTotalFeesNoTax());
            $purProd->setPurchase($purchase);
            $purProd->setName($product->getName());
            $purProd->setIncludeFeesWithTax($product->getTotalFeesWithTax());
            $purProd->setShortDescription($product->getShortDescription());
            $newStatus=$this->em->getRepository("AppBundle:Status")->findOneBy(array("name"=>"new"));
            $purProd->setStatus($newStatus);
            if($product->getTax()){
                $purProd->setTaxRate($product->getTax()->getRate());
            }
            $purProd->setUnitPriceNoTax($product->getTotalPriceNoTax());
            $purProd->setWarranty($product->getWarranty());
            $this->em->persist($purProd);
            $this->em->flush();
        }
    }

    /**
     * Vytváří objednávku a zapisuje ji do databáze
     * @param User $user
     * @return bool
     */
    public function createPurchase(User $user=null){
        if($user){
            $user=$this->em->getRepository("AppBundle:User")->find($user->getId());
        }
        $purchase=$this->session->get("purchase")["class"];
        $invoice=$this->session->get("invoice")["class"];
        $contact=$this->session->get("contact")["class"];
        if($purchase && $purchase->getDelivery() && $purchase->getPayment() && $invoice && $contact){
            $purchase->setContact($contact);
            $contact->setPurchase($purchase);
            $purchase->setDelivery($this->em->getRepository("AppBundle:Delivery")->find($purchase->getDelivery()->getId()));
            $purchase->setPayment($this->em->getRepository("AppBundle:Payment")->find($purchase->getPayment()->getId()));
            $purchase->setInvoiceAddress($invoice);
            $invoice->setInvoicePurchase($purchase);
            if($this->session->get("company") && $this->session->get("company")["class"]->getName()){
                $company=$this->session->get("company")["class"];
                $purchase->setCompany($company);
                $company->setPurchase($purchase);
                if($user){
                    $company->setUser($user);
                }
            }
            if($this->session->get("delivery")["class"]){
                $delivery=$this->session->get("delivery")["class"];
                $purchase->setDeliveryAddress($delivery);
                $delivery->setDeliveryPurchase($purchase);
                if($user){
                    $delivery->setDeliveryUser($user);
                }
            }
            if($user){
                $purchase->setUser($user);
                $invoice->setInvoiceUser($user);
                $contact->setUser($user);;
            }
        }else{
            return false;
        }
        /*
         *
         */
        $this->em->persist($contact);
        $this->em->persist($invoice);
        $this->em->persist($purchase);
        $this->em->flush();
        $this->setPurchaseProduct($purchase);
        return true;
    }
}