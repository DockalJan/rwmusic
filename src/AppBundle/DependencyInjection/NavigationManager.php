<?php
/**
 * Created by PhpStorm.
 * User: Honzik
 * Date: 28.4.2015
 * Time: 12:22
 */

namespace AppBundle\DependencyInjection;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;

/**
 * Třída pracující s navigací na stránce návratové hodnoty formou stringu s html tagy.
 * Představuje paginator a také navigační menu
 * Class NavigationManager
 * @package AppBundle\DependencyInjection
 */
class NavigationManager {

    private $repo;
    private $router;
    private $limit;

    public function __construct(EntityManager $em,Router $router){
        $this->repo=$em->getRepository("AppBundle:Category");
        $this->router=$router;
        $this->limit=3;
    }

    /**
     * @param $limit
     */
    public function setLimit($limit){
        $this->limit=$limit;
    }

    /**
     * @return int
     */
    public function getLimit(){
        return $this->limit;
    }

    /**
     * Vytváří navigační menu stránky
     * @return array|string
     */
    public function createHtmlNavigation(){
        return $this->repo->childrenHierarchy(
            null,
            false,
            array(
                'decorate' => true,
                'rootOpen' => function($node){
                    $level="level-".$node[0]["lvl"];
                    if($node[0]["lvl"]!=0){
                        return "<ul id='category-".$node[0]['id']."' style='display:none;' class='".$level."'>";
                    }
                    return "<ul id='category-".$node[0]['id']."' class='".$level."'>";
                },
                'rootClose' => '</ul>',
                'childOpen' => '<li>',
                'childClose' => '</li>',
                'nodeDecorator' => function($node) {
                    $category=$this->repo->find($node["id"]);
                    $pathTree=$this->repo->getPath($category);
                    $categoriesUrl="";
                    foreach($pathTree as $value){
                        $page=$value->getPage();
                        if(!$page){
                            throw new NotFoundHttpException("Není nalezen záznam o stánce na tuto kategorii: ".$node["name"]);
                        }
                        $categoriesUrl.=$page->getAddress()."/";
                    }
                    $categoriesUrl=rtrim($categoriesUrl,"/");
                    $path=$this->router->generate("eshop_show_categoy",array("category"=>$categoriesUrl,"limit"=>$this->getLimit(),"offset"=>0));
                    return '<a href="'.$path.'">'.$node["name"].'</a>';
                }
            )
        );
    }


    /**
     * Paginator na stránce
     * @param $category
     * @param $limit
     * @param $offset
     * @param $totalProducts
     * @return string
     */
    public function crateHtmlPagination($category,$limit,$offset,$totalProducts){
        $html="";
        $this->setLimit($limit);
        $i=1;
        $o=0;
        $offset=intval($offset);
        if($offset-$limit>=0){
            $prev=$this->router->generate("eshop_show_categoy",array("category"=>$category,"limit"=>$limit,"offset"=>$offset-$limit));
            $html.="<a class='noNavigationChange' href='".$prev."' style='padding-right: 5px '><span class='glyphicon glyphicon-chevron-left'></span>Předchozí strana</a>";
        }
        while($o<$totalProducts):
            $route=$this->router->generate("eshop_show_categoy",array("category"=>$category,"limit"=>$limit,"offset"=>$o));
            if($offset===$o){
                $html.="<strong style='padding-right: 5px '> $i </strong>";
            }else{
                $html.="<a class='noNavigationChange' href='".$route."' style='padding-right: 5px '> ".$i." </a>";
            }
            $o+=$limit;
            $i++;
        endwhile;
        if($offset+$limit<$totalProducts){
            $next=$this->router->generate("eshop_show_categoy",array("category"=>$category,"limit"=>$limit,"offset"=>$offset+$limit));
            $html.="<a class='noNavigationChange' href='".$next."' style='padding-right: 5px '>Další strana<span class='glyphicon glyphicon-chevron-right'></span></a>";
        }
        return $html;
    }
}