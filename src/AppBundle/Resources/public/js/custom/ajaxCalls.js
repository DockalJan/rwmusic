/**
 * Created by Honzik on 29.4.2015.
 */
$(document).ready(function(){
    var navigationString="navigation-";
    var topNavigation=$('*[id^='+navigationString+']');
    var forms=$('*[id^="final-"]');
    var backButton=$(".back-form-button");
    var pasteErrors=$("#paste-errors");
    var nextButtons=$(".next-button");
    var final=$("#finalSent");


    var checkboxes=$('*[id^="checkbox-input-"]');
    var checkboxesForms=$('*[id^="checkbox-form-base-"]');
    var checkboxesFormsAnti=$('*[id^="checkbox-form-anti-"]');

    var unsetProperty=function(property){
        $.ajax({
            type:'POST',
            url:property
        }).done(function(result){
            console.log(result);
            if(result.status==201){
                pasteErrors.empty();
            }
        });
    };

    final.on("click",function(){
       summary();
    });
    var setOnClick= function(i){
        $(checkboxes[i]).on("click",function(){
            if(this.checked){
                console.log("tudy");
                pasteErrors.empty();
                $.totalStorage("checked-"+[i],true);
                $(checkboxesForms[i]).fadeIn();
                $(checkboxesFormsAnti[i]).hide();
            }else{
                console.log("tudy2");
                unsetProperty($(checkboxes[i]).attr("about"));
                $.totalStorage("checked-"+[i],false);
                $(checkboxesForms[i]).hide();
                $(checkboxesFormsAnti[i]).fadeIn();
            }
        });
    };

    for(var i=0; i<checkboxes.length;i++){
        var checked=$.totalStorage("checked-"+[i]);
        if(checked==true){
            $(checkboxes[i]).attr("checked",true);
            $(checkboxesForms[i]).show();
            $(checkboxesFormsAnti[i]).hide();
            $.totalStorage("checked-"+[i],true);
        }else{
            $(checkboxes[i]).attr("checked",false);
            $(checkboxesForms[i]).hide();
            $(checkboxesFormsAnti[i]).show();
            $.totalStorage("checked-"+[i],false);
            unsetProperty($(checkboxes[i]).attr("about"));
        }
        setOnClick(i);
    }



    backButton.on("click",function(){
        back();
    });

    nextButtons.on("click",function(){
        right();
    });

    var setActual=function(id){
        id=parseInt(id);
        $.totalStorage("actual-payment",id);
    };

    var getActual=function(){
        if(!$.totalStorage("actual-payment")){
            $.totalStorage("actual-payment",1);
        }
        return parseInt($.totalStorage("actual-payment"));
    };

    var inProgress=false;
    var ajaxSubmit=function(form,last){
        form.submit(function(event){
            event.preventDefault();
            if(inProgress){
                return;
            }
            inProgress=true;
            var formData=$(form).serialize();
            return $.ajax({
                type:'POST',
                url:$(form).attr('action'),
                data:formData
            }).done(function(response){
                inProgress=false;
                pasteErrors.empty();
                console.log(response);
                if(response.status==201){
                    console.log("going to right");
                    if(!last){
                        right();
                    }else{
                        summary();
                    }
                }else{
                    if(response.status=400){
                        var errors="";
                        for(var i = 0; i < response.errorReport.length; i++){
                            errors+="<li>"+response.errorReport[i]+"</li>";
                        }
                        pasteErrors.append(errors);
                        pasteErrors.show();
                    }else{
                    }
                }
            })
        })
    };

    var summary=function(){
        $.ajax({
            type:"POST",
            url:$(final).attr("about")
        }).done(function(result){
            console.log(result);
            $.totalStorage("actual-payment",1);
            if(result.status==201){
                $("#successRedirect").fadeIn();
                forms.hide();
                topNavigation.hide();
            }else{
                pasteErrors.append("<li>Nastala chyba během ukládání dat, " +
                "zkontrolujte prosím objednvací formulář</li>");
                pasteErrors.show();
            }
        });
    };

    var back=function(){
        var currentId=getActual();
        if(currentId-1>0){
            setActual(currentId-1);
            showForm();
            pasteErrors.empty();
        }
    };

    var right=function(){
        backButton.show();
        var currentId=getActual();
        if(currentId+1<=forms.length){
            setActual(currentId+1);
            showForm();
        }
    };

    var showForm=function(){
        for (var i=0;i<forms.length;i++){
            var form=forms[i];
            var idName=$(form).attr("id");
            var id=idName.substr(idName.length-1);
            id=parseInt(id);
            if(id<getActual()){
                $(topNavigation[i]).attr("class","btn btn-success");
                $(form).hide();
            }
            if(id===getActual()){
                if(id-1<=0){
                    backButton.hide();
                }
                $(topNavigation[i]).attr("class","btn btn-primary");
                $(form).show();
                $(form).unbind("submit");
                if(i+1==forms.length){
                    ajaxSubmit($(form),true);
                }
                ajaxSubmit($(form),false);
                setActual(id);
            }
            if(id>getActual()){
                $(topNavigation[i]).attr("class","btn btn-warning");
                $(form).hide();
            }
        }
    };
    showForm();

});
