<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Purchase;
use AppBundle\Form\AddressType;
use AppBundle\Form\CompanyType;
use AppBundle\Form\PurchaseType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends Controller
{
    /**
     * Metoda obsluhující URL ajax/delivery
     * Přijíma pouze ajax requesty. Je součástí vyplňování údajů objednávky. Dodací údaje
     * @param Request $request
     * @return Response
     */
    public function deliveryAction(Request $request){
        $address= new Address();
        $form=$this->createForm(new AddressType(),$address);
        if($request->isXmlHttpRequest()){
            $form->handleRequest($request);
            if($form->isValid()){
                $array=array( 'status' => 201, 'msg' => 'Form valid');
                $request->getSession()->set("delivery",array("class"=>$address,"code"=>201));
            }else{
                $request->getSession()->set("delivery",array("class"=>$address,"code"=>400));
                $errors=$form->getErrors(true,true);
                $errorCollection = array();
                foreach($errors as $error){
                    $errorCollection[] = $error->getMessage();
                }
                $array = array( 'status' => 400, 'errorMsg' => 'Bad Request', 'errorReport' => $errorCollection);
            }
        }else{
            $array= array('status' => 401,'data' => 'this is not a json response');
        }
        $response = new Response( json_encode( $array ) );
        $response->headers->set( 'Content-Type', 'application/json' );

        return $response;
    }

    /**
     * Metoda obsluhující URL ajax/invoice
     * Přijíma pouze ajax requesty. Je součástí vyplňování údajů objednávky. Fakturační adresa
     *
     * @param Request $request
     * @return Response
     */
    public function invoiceAction(Request $request){
        $address= new Address();
        $form=$this->createForm(new AddressType(),$address);
        if($request->isXmlHttpRequest()){
            $form->handleRequest($request);
            if($form->isValid()){
                $array=array( 'status' => 201, 'msg' => 'Form valid');
                $request->getSession()->set("invoice",array("class"=>$address,"code"=>201));
            }else{
                $request->getSession()->set("invoice",array("class"=>$address,"code"=>400));
                $errors=$form->getErrors(true,true);
                $errorCollection = array();
                foreach($errors as $error){
                    $errorCollection[] = $error->getMessage();
                }
                $array = array( 'status' => 400, 'errorMsg' => 'Bad Request', 'errorReport' => $errorCollection);
            }
        }else{
            $array= array('status' => 401,'data' => 'this is not a json response');
        }
        $response = new Response( json_encode( $array ) );
        $response->headers->set( 'Content-Type', 'application/json' );

        return $response;
        }

    /**
     * Metoda obsluhující URL ajax/contact
     * Přijíma pouze ajax requesty. Je součástí vyplňování údajů objednávky. Kontaktní spojení
     *
     * @param Request $request
     * @return Response
     */
    public function contactAction(Request $request){
        $contact= new Contact();
        $form=$this->createForm(new \AppBundle\Form\ContactType(),$contact);
        if($request->isXmlHttpRequest()){
            $form->handleRequest($request);
            if($form->isValid()){
                $array=array( 'status' => 201, 'msg' => 'Form valid');
                $request->getSession()->set("contact",array("class"=>$contact,"code"=>201));
            }else{
                $request->getSession()->set("contact",array("class"=>$contact,"code"=>400));
                $errors=$form->getErrors(true,true);
                $errorCollection = array();
                foreach($errors as $error){
                    $errorCollection[] = $error->getMessage();
                }
                $array = array( 'status' => 400, 'errorMsg' => 'Bad Request', 'errorReport' => $errorCollection);
            }
        }else{
            $array= array('status' => 401,'data' => 'this is not a json response');
        }
        $response = new Response( json_encode( $array ) );
        $response->headers->set( 'Content-Type', 'application/json' );

        return $response;
        }

    /**
     *
     * Metoda obsluhující URL ajax/company
     * Přijíma pouze ajax requesty. Je součástí vyplňování údajů objednávky. Údaje o společnosti
     * @param Request $request
     * @return Response
     */
    public function companyAction(Request $request){
        $company= new Company();
        $form=$this->createForm(new CompanyType(),$company);
        if($request->isXmlHttpRequest()){
            $form->handleRequest($request);
            if($form->isValid()){
                $array=array( 'status' => 201, 'msg' => 'Form valid');
                $request->getSession()->set("company",array("class"=>$company,"code"=>201));
            }else{
                $request->getSession()->set("company",array("class"=>$company,"code"=>400));
                $errors=$form->getErrors(true,true);
                $errorCollection = array();
                foreach($errors as $error){
                    $errorCollection[] = $error->getMessage();
                }
                $array = array( 'status' => 400, 'errorMsg' => 'Bad Request', 'errorReport' => $errorCollection);
            }
        }else{
            $array= array('status' => 401,'data' => 'this is not a json response');
        }
        $response = new Response( json_encode( $array ) );
        $response->headers->set( 'Content-Type', 'application/json' );

        return $response;
        }

    /**
     *
     * Metoda obsluhující URL ajax/purchase
     * Přijíma pouze ajax requesty. Je součástí vyplňování údajů objednávky. Vybírání typu dopravy a platby
     *
     * @param Request $request
     * @return Response
     */
    public function purchaseAction(Request $request){
        $purchase= new Purchase();
        $form=$this->createForm(new PurchaseType(),$purchase);
        if($request->isXmlHttpRequest()){
            $form->handleRequest($request);
            if($form->isValid()){
                $array=array( 'status' => 201, 'msg' => 'Form valid');
                $request->getSession()->set("purchase",array("class"=>$purchase,"code"=>201));
            }else{
                $request->getSession()->set("purchase",array("class"=>$purchase,"code"=>400));
                $errors=$form->getErrors(true,true);
                $errorCollection = array();
                foreach($errors as $error){
                    $errorCollection[] = $error->getMessage();
                }
                $array = array( 'status' => 400, 'errorMsg' => 'Bad Request', 'errorReport' => $errorCollection);
            }
        }else{
            $array= array('status' => 401,'data' => 'this is not a json response');
        }
        $response = new Response( json_encode( $array ) );
        $response->headers->set( 'Content-Type', 'application/json' );

        return $response;
    }

    /**
     * Metoda obsluhující URL ajax/unset-delivery
     * Přijíma pouze ajax requesty. Je součástí vyplňování údajů objednávky.
     * Metoda odstaraňuje z proměnné session doručovací adresu
     *
     *
     * @param Request $request
     * @return Response
     */
    public function unsetDeliveryAction(Request $request){
        if($request->isXmlHttpRequest()){
            $request->getSession()->set("delivery",null);
            $array=array( 'status' => 201, 'msg' => 'Delivery was unset ');
        }else{
            $array= array('status' => 401,'data' => 'this is not a json response');
        }
        $response = new Response( json_encode( $array ) );
        $response->headers->set( 'Content-Type', 'application/json' );

        return $response;
    }


    /**
     *
     * Metoda obsluhující URL ajax/unset-company
     * Přijíma pouze ajax requesty. Je součástí vyplňování údajů objednávky.
     * Metoda odstaraňuje z proměnné session firmu
     *
     * @param Request $request
     * @return Response
     */
    public function unsetCompanyAction(Request $request){
        if($request->isXmlHttpRequest()){
            $request->getSession()->set("company",null);
            $array=array( 'status' => 201, 'msg' => "Company was unset");
        }else{
            $array= array('status' => 401,'data' => 'this is not a json response');
        }
        $response = new Response( json_encode( $array ) );
        $response->headers->set( 'Content-Type', 'application/json' );

        return $response;
    }


    /**
     * Metoda obsluhující URL ajax/summary
     * Přijíma pouze ajax requesty. Je součástí vyplňování údajů objednávky.
     * Kompletně vyřizuje a zpracovává objednávku
     *
     * @param Request $request
     * @return bool|Response
     */
    public function summaryAction(Request $request){
        $response=$this->get("cart")->createPurchase($this->getUser());
        if($response){
            $request->getSession()->clear();
            $array=array("status"=>201,"msg"=>"Objednávka byla přijata");
        }else{
            $array= array('status' => 401,'data' => 'Chyba');
        }
        $response= new Response(json_encode($array));
        $response->headers->set( 'Content-Type', 'application/json' );
        return $response;
    }

}
