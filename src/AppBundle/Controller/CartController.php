<?php
/**
 * Created by PhpStorm.
 * User: Honzik
 * Date: 13.4.2015
 * Time: 12:17
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Delivery;
use AppBundle\Entity\Purchase;
use AppBundle\Form\AddressType;
use AppBundle\Form\CompanyType;
use AppBundle\Form\ContactType;
use AppBundle\Form\PurchaseType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CartController extends Controller {


    /**
     * Metoda obsluhující URL /cart/add-to-cart/{product}
     * Přidává id zboží produktu do košíku
     *
     * @param $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction($product){
        $cart=$this->get("cart");
        $cart->addProduct($product,1);
        return $this->redirectToRoute("eshop_show_cart",array(),301);
    }


    /**
     * Zobrazuje Produkty v košíku
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(){
        $repo=$this->getDoctrine()->getRepository("AppBundle:Product");
        $cart=$this->get("cart");
        $products=$cart->getProducts();
        $cartProducts=Array();
        foreach($products as $key=>$product){
            $prod=$repo->find($product["product"]);
            $q=$product["quantity"];
            $images=$prod->getImages();
            $mainImage="";
            foreach($images as $image){
                if($image->getPlacings()===1){
                    $mainImage=$image->getName();
                }
            }
            $form=$this->createFormBuilder(null)
                ->add("product","hidden",array("data"=>$product["product"]))
                ->add("quantity","number",array("data"=>$q))
                ->getForm();
            array_push($cartProducts,array("product"=>$prod,"quantity"=>$q,"image"=>$mainImage,"form"=>$form->createView()));
        }
        $test=$repo->find(3);
        return $this->render("AppBundle:Cart:cart.html.twig",array("cart"=>$cartProducts,"test"=>$test));
    }

    /**
     * Nastavuje množtví produktů v košíku
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setQuantityAction(Request $request){
        $form=$this->createFormBuilder(null)
            ->add("product","hidden")
            ->add("quantity","number")
            ->getForm();
        $form->handleRequest($request);
        if($form->isValid()){
            $cart=$this->get("cart");
            $data=$form->getData();
            if($data["quantity"]<0){
                $this->addFlash(
                    "error",
                    "Počet nesmí být záporný"
                );
                return $this->redirectToRoute("eshop_show_cart");
            }
            if($request->request->has("delete")||$data["quantity"]===0){
                $cart->removeProduct($data["product"]);
            }else{
                $cart->setQuantity($data["product"],$data["quantity"]);
            }
            return $this->redirectToRoute("eshop_show_cart");
        };
        $this->addFlash(
            "error",
            $form->getErrorsAsString()
        );
        return $this->redirectToRoute("eshop_show_cart");
    }


    /**
     * Vykresluje jednotlivé formuláře související s dokončením objednávky
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function paymentDeliveryAction(Request $request){
        if($this->get("cart")->isEmpty()){
            return $this->redirectToRoute("eshop_main_homepage");
        }
        $addressInvoice= new Address();
        $contact= new Contact();
        $addressDelivery= new Address();
        $company=new Company();
        $purchase= new Purchase();
        if($this->getUser()){
            $contact->setEmail($this->getUser()->getEmail());
            $addressDelivery=$this->getUser()->getDeliveryAddress();
            $addressDelivery=$addressDelivery[sizeof($addressDelivery)-1];
            $addressInvoice=$this->getUser()->getInvoiceAddress();
            $addressInvoice=$addressInvoice[sizeof($addressInvoice)-1];
            $contact=$this->getUser()->getContacts()[0];
            $company=$this->getUser()->getCompanies()[0];
        }
        if($request->getSession()->get("delivery")){
            $addressDelivery=$request->getSession()->get("delivery")["class"];
        }
        if($request->getSession()->get("invoice")){
            $addressInvoice=$request->getSession()->get("invoice")["class"];
        }
        if($request->getSession()->get("contact")){
            $contact=$request->getSession()->get("contact")["class"];
        }
        if($request->getSession()->get("company")){
            $company=$request->getSession()->get("company")["class"];
        }
        if($request->getSession()->get("purchase")){
            $idDelivery=$request->getSession()->get("purchase")["class"]->getDelivery()->getId();
            $idPayment=$request->getSession()->get("purchase")["class"]->getPayment()->getId();
            $purchase->setDelivery($this->getDoctrine()->getRepository("AppBundle:Delivery")->find($idDelivery));
            $purchase->setPayment($this->getDoctrine()->getRepository("AppBundle:Payment")->find($idPayment));
        }
        $contactForm=$this->createForm(new ContactType(),$contact);
        $deliveryForm=$this->createForm(new AddressType(),$addressDelivery);
        $invoiceForm=$this->createForm(new AddressType(),$addressInvoice);
        $companyForm=$this->createForm(new CompanyType(),$company);
        $purchaseForm=$this->createForm(new PurchaseType(),$purchase);

        return $this->render("AppBundle:Cart:payment-final.html.twig",
            array(
                "deliveryForm"=>$deliveryForm->createView(),
                "contactForm"=>$contactForm->createView(),
                "invoiceForm"=>$invoiceForm->createView(),
                "companyForm"=>$companyForm->createView(),
                "purchaseForm"=>$purchaseForm->createView()
            ));
    }

}