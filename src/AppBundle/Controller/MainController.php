<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\Category;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MainController extends Controller
{

    /**
     * Homepage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Main:index.html.twig');
    }

    /**
     * Slouží pro vypsiování příslušných stránek. Jako je o nás, kontankt atd..
     * v parametru address se nachází page, podle které se vyhledá příslušný záznam z databáze
     * @param $address
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws EntityNotFoundException
     */
    public function infoAction($address){
        $page=$this->getDoctrine()->getRepository("AppBundle:Page")->findOneBy(array("address"=>$address));
        if(!$page || !$page->getArticle()){
            throw new EntityNotFoundException("Nebyl nazelen záznam o stránce nebo příslušný článek");
        }
        $content=$page->getArticle()->getContent();
        $name=$page->getArticle()->getName();
        return $this->render("@App/Main/info.html.twig",array("content"=>$content,"name"=>$name,"meta"=>$page));
    }

    /**
     * Zobrazuje detail produktu
     * @param $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productAction($product){
        $page=$this->getDoctrine()->getRepository("AppBundle:Page")->findOneBy(array("address"=>$product));
        if(!$page){
            throw new NotFoundHttpException("Nenalezeno ".$product);
        }
        $product=$page->getProduct();
        return $this->render('AppBundle:Main:product.html.twig',array("product"=>$product));
    }


    /**
     * Metoda zpracovává vybranou kategorii
     * a vrací pole produktů obsažených v ní a v kategoriéch vnořených
     * @param $category
     * @param $limit
     * @param $offset
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction($category,$limit,$offset){
        $categories=explode("/",$category);
        $last=array_pop($categories);
        if($last===""){
            $last=$categories[sizeof($categories)-1];
        }
        $page=$this->getDoctrine()->getRepository("AppBundle:Page")->findOneBy(array("address"=>$last));
        $categoryRepo=$this->getDoctrine()->getRepository("AppBundle:Category");
        $productRepo=$this->getDoctrine()->getRepository("AppBundle:Product");
        if(!$page){
            throw new NotFoundHttpException("Nenalezeno ".$last);
        }
        $categoryClass=$categoryRepo->find($page->getCategory());
        $categories=$categoryRepo->getChildren($categoryClass);
        array_push($categories,$categoryClass);
         $catIds=[];
        foreach($categories as $cat){
            array_push($catIds,$cat->getId());
        }
        $products=$productRepo->getProductsByCategory($catIds,$limit,$offset);
        $totalProducts=intval($productRepo->countProductsInCategories($catIds));
        $html=$this->get("navigation_manager")->crateHtmlPagination($category,$limit,$offset,$totalProducts);
        return $this->render("@App/Main/category.html.twig",array("products"=>$products,"category"=>$last,
            "meta"=>$page,"pagination"=>$html,"limit"=>$limit,"offset"=>$offset,"total"=>$totalProducts));
    }
}
