<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname','text',array("label"=>"Křestní jméno","required"=>false))
            ->add('lastname','text',array("label"=>"Příjmení","required"=>false))
            ->add('city','text',array("label"=>"Město","required"=>false))
            ->add('street','text',array("label"=>"Ulice","required"=>false))
            ->add('zipCode','text',array("label"=>"PSČ","required"=>false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Address'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_address';
    }
}
