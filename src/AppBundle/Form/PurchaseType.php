<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PurchaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('delivery',"entity",array(
                'label'=>'Způsob dopravy',
                "class"=>'AppBundle\Entity\Delivery',
                'expanded'=>true,
                'choices'=>false
            ))
            ->add('payment','entity',array(
                'label'=>'Způsob platby',
                'class'=>'AppBundle\Entity\Payment',
                'expanded'=>true,
                'choices'=>false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Purchase'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_purchase';
    }
}
